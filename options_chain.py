#! /usr/bin/python
# get one ticker at a time, then run the command against the given ticker.
# kyurimrhee 
# 2016.08.07

import subprocess
import datetime

PATH = "/Users/kyurim/Desktop/options-chain"

def main():
	# Get Timestamp for start of script
	startTime = datetime.datetime.now();
	print "\n\n"
	print "********************************************************************************************"
	print "    options_chain.py script started:  " + str(startTime)
	print "********************************************************************************************"

	# Lynx text web-browser gets the dates of ticker.  Returns ['date Ticker1', 'date Ticker2' ...]
	txt = open(PATH + "/ticker.list" ,'r')

	for ticker in txt:							# get a single line.  Each line is a ticker.
		print "------------------------------------------------------------"
		ticker = ticker[:len(ticker)-1];		#print ticker
		
		# for the given ticker, run options_chain_exp_date.py.  Get all expiration dates
		CMD = PATH + '/options_chain_exp_date.py ' + ticker; 		print CMD
		p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
		(output, err) = p.communicate()
		print output

		expDate = output.strip().split(', '); 	#print expDate

		# for all expiration date, run options_chain_contr_to_db.py.  Get all contracts info and put into db.
		for eDate in expDate:
			CMD = PATH + '/options_chain_contr_to_db.py ' + ticker + ' ' + eDate; 		print CMD
			p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
			(output, err) = p.communicate()
			# print output

	txt.close()

	# Get Database file size
	CMD = 'ls -lh ' + PATH + '/database  | grep .db';		#print CMD
	p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
	(db_size, err) = p.communicate()

	CMD = 'ls -lh ' + PATH + ' | grep .log';		#print CMD
	p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
	(log_size, err) = p.communicate()

	# Logging
	# Get Timestamp for end of script
	endTime = datetime.datetime.now();

	print ""
	print "********************************************************************************************"
	print "    options_chain.py script started:  " + str(startTime)
	print "                     script ended:    " + str(endTime)
	print "                     script duration:             " + str(endTime - startTime)
	print ""
	print "    database size: "
	print "      " + db_size
	print ""
	print "    log size: "
	print "      " + log_size
	print "********************************************************************************************"

if __name__ == "__main__":
	main()
