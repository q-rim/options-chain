#! /usr/bin/python
# parse Google Finance Options Chain
# kyurimrhee 
# 2016.08.07

import string
import subprocess
import sys


def getExpirationDatesStrLine(lines):
	for i in range(len(lines)):
		line = lines[i]
		# print "-----line" + str(i) + "-----:     ", line

		# GET EXPIRATION DATES
		# if ("<span id" == line[0:8]) and ("<span id=" in line):
		if "option_chain:{expiry:" in line:
			# print "-----line" + str(i) + "-----:     ", line

			# extract Expiration Date
			temp, expDates = line.split(",expirations:[", 1);		#print expDates 	# split only once (extract after keyword)	
			expDates, temp = expDates.split("]", 1);				#print expDates  	# split only once (extract before keyword, rest)

	expDates = string.replace(expDates, "y:", "");					#print expDates
	expDates = string.replace(expDates, ",m:", ".");				#print expDates
	expDates = string.replace(expDates, ",d:", ".");				#print expDates
	expDates = string.replace(expDates, "},{", ", ");				#print expDates
	expDates = string.replace(expDates, "{", "");					#print expDates
	expDates = string.replace(expDates, "}", "");					#print expDates
	# print expDates
	return expDates

def expirationDatesStrLineToArray(strLine):
	expDateArr = strLine.split(', ');			# create array of lines of contract
	# expDateArr = expDateArr[1:];			# index=0 is null, snip it off
	# print expDateArr
	return expDateArr

def main():
	# Parse html file.

	tickerURL = str(sys.argv[1])
	CMD = 'wget -qO- "https://www.google.com/finance/option_chain?q=' + tickerURL + '"'; 		#print CMD
	p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	# print output

	# get lines of html.  lines = ['Line1', 'Line2', 'Line3'] 
	html_lines = output.strip().split('\n'); 	#print html_lines	
	
	# Create a Array with basic info that will be prepended to all contract
	expDateStrLine = getExpirationDatesStrLine(html_lines)
	print expDateStrLine
	expDatesArray = expirationDatesStrLineToArray(expDateStrLine)
	# print expDatesArray

if __name__ == "__main__":
	main()
