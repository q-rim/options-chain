#! /usr/bin/python
# SQL Command Options
# kyurimrhee 
# 2016.08.07

import sqlite3
import sys

def sqlCreateTable():
	# Add contract into SQLite db
	print "---- Create Database ----"
	cmd = """CREATE TABLE Options(time_sys TEXT, 
											time_web TEXT, 
											ticker TEXT,
											stock_price TEXT, 
											call_put TEXT, 
											cid TEXT, 
											name TEXT, 
											sGoogle TEXT, 
											eGoogle TEXT, 
											price TEXT, 
											csGoogle TEXT, 
											change TEXT, 
											buy TEXT, 
											ask TEXT, 
											openInt TEXT, 
											volume TEXT, 
											strike TEXT, 
											expDate TEXT);"""
	print "SQL> " + cmd + "\n"
	connection = sqlite3.connect('database/options.db')		# create connection to db
	curser = connection.cursor()							# create curser
	curser.execute(cmd)


def sqlPrint():
	# Add contract into SQLite db
	print "---- Print Database ----"
	cmd = "SELECT * FROM Options"
	print "SQL> " + cmd + "\n"
	print "print row by row"
	connection = sqlite3.connect('database/options.db')		# create connection to db
	curser = connection.cursor()							# create curser
	curser.execute(cmd)

	# Print SQLite Query
	data = curser.fetchall()
	i = 1
	for row in data:
		print "row "+str(i)+ ": " , row
		i +=1


def sqlDropTable():
	print "---- Dropping Database ----"
	cmd = "DROP TABLE IF EXISTS Options"
	print "SQL> " + cmd + "\n"

	connection = sqlite3.connect('database/options.db')		# create connection to db
	curser = connection.cursor()							# create curser
	curser.execute(cmd)


def main():
	if len(sys.argv) == 2:
		user_selection = str(sys.argv[1])
	
	else:
		user_selection = raw_input("""

---------- SQL COMMAND ----------
 
 1. c. Create - create new talble         ::: SQL> CREATE TABLE Options(time_sys TEXT, time_web TEXT, ..., strike TEXT, expDate TEXT);
 2. d. Drop - Drop table                  ::: SQL> DROP TABLE IF EXISTS Options
 3. p. Print - print current table        ::: SQL> SELECT * FROM Options;  then print row by row via Python.

Enter your selection (1-3):   """)
	if user_selection not in ["1", "2", "3", "c", "d", "p"]:
		print "> input error\n"; 	
		exit()

	# print "your selection: " + user_selection

	if user_selection == "1" or user_selection == "c":	
		sqlCreateTable()

	if user_selection == "2" or user_selection == "d":
		sqlDropTable()

	if user_selection == "3" or user_selection == "p":
		sqlPrint()

	# # Executing Multiple SQLite Commands from Python
	# curser.executescript("""DROP TABLE IF EXISTS Options;
	# 					CREATE TABLE Options(time_sys TEXT, 
	# 										time_web TEXT, 
	# 										ticker TEXT,
	# 										stock_price TEXT, 
	# 										call_put TEXT, 
	# 										cid TEXT, 
	# 										name TEXT, 
	# 										sGoogle TEXT, 
	# 										eGoogle TEXT, 
	# 										price TEXT, 
	# 										csGoogle TEXT, 
	# 										change TEXT, 
	# 										buy TEXT, 
	# 										ask TEXT, 
	# 										openInt TEXT, 
	# 										volume TEXT, 
	# 										strike TEXT, 
	# 										expDate TEXT);
	# 					""")

	# # Create a final contract Array & Put into SQLite database
	# for i in range(len(contractsStr)-1):
	# 	# execute SQLite query
	# 	curser.execute("INSERT INTO Options VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", preArr + contractStrToArr(contractsStr[i]))	

	# connection.commit()

if __name__ == '__main__':
	main() 
