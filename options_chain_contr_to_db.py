#! /usr/bin/python
# parse Google Finance Options Chain
# kyurimrhee 
# 2016.08.07

# To run this script:
# > options_chain_contr_to_db.py <arg1> <arg2>
# <arg1> = NASDAQ:NFLX         <-- Ticker
# <arg2> = 2016.10.21          <-- Contract Expiration Date
# EX:  
# > ./options_chain_contr_to_db.py NASDAQ:NFLX 2016.10.21

import string
import datetime
import subprocess
import sqlite3
import sys;

PATH = "/Users/kyurim/Desktop/options-chain"

def getTime():
	# GET CURRENT TIME from System
	# Returns Timestamp in ISO format:  
	# 2016-09-21 15:06
	time = datetime.datetime.now()			#print time
	# yy, time = str(time).split('-', 1); 	#print yy
	# mo, time = time.split('-', 1); 			#print mo
	# dd, time = time.split(' ', 1); 			#print dd
	# hh, time = time.split(':', 1); 			#print hh
	# mm, time = time.split(':', 1); 			#print mm
	# time = hh + ":" + mm;							#print time
	# print "current time:  " + time
	time, temp = str(time).rsplit(":", 1)
	# print time
	return time


def getTickerTimestampStockPriceArr(lines):
	for i in range(len(lines)):
		line = lines[i]
		# print "-----line" + str(i) + "-----:     ", line

		# GET TIMESTAMP
		if "<span class=time id=" in line:
			# print "-----line" + str(i) + "-----:     ", line

			# extract timestamp from line
			temp, timestamp = line.split('>', 1);			# print timestamp 	# split only once (extract after >)	
			timestamp, temp = timestamp.split('<', 1);		# print timestamp 	# split only once (extract before <, rest)

		# GET STOCK PRICE
		if ("<span id" == line[0:8]) and ("<span id=" in line):
			# print "-----line" + str(i) + "-----:     ", line

			# extract ticker from line
			temp, stockPrice = line.split('>', 1);			# print stockPrice 	# split only once (extract after >)	
			stockPrice, temp = stockPrice.split('<', 1);	# print stockPrice  # split only once (extract before <, rest)

	# print "Ticker:  " + ticker
	# print "Time stamp:  " + timestamp
	# print "Stock Price:  " + stockPrice
	return [timestamp, stockPrice]


def contractStrToArr(contract):
	# Takes a string of line of contract
	# returns array:  [sGoogle, price, buy, ask, openInt, volume]

	temp, putOrCall = contract.split('"', 1);  	
	putOrCall, temp = putOrCall.split('",', 1);			#print "putOrCall: ",  putOrCall	

	temp, cid = contract.split(', "', 1);  	
	cid, temp = cid.split('",', 1);						#print "cid: ",  cid

	# get sGoogle (contract)
	if ',s:"' in contract:
		temp, sGoogle = contract.split(',s:"', 1); 			
		sGoogle, temp = sGoogle.split('",', 1);			#print "s: ", sGoogle
	else:
		sGoogle = ""

	# get price
	if ',p:"' in contract:
		temp, price = contract.split(',p:"', 1); 			
		price, temp = price.split('",', 1);				#print "p(price): ", price
	else:
		price = ""

	# get buy
	if ',b:"' in contract:
		temp, buy = contract.split(',b:"', 1); 			
		buy, temp = buy.split('",', 1);					#print "b(buy): ", buy
	else:
		buy = ""

	# get ask
	if ',a:"' in contract:
		temp, ask = contract.split(',a:"', 1); 			
		ask, temp = ask.split('",', 1);					#print "a(ask): ", ask
	else:
		ask = ""

	# get openInt
	if ',oi:"' in contract:
		temp, openInt = contract.split(',oi:"', 1); 			
		openInt, temp = openInt.split('",', 1);			#print "oi(openInt): ", openInt
	else:
		openInt = ""

	# get volume
	if ',vol:"' in contract:
		temp, volume = contract.split(',vol:"', 1); 			
		volume, temp = volume.split('",', 1);			#print "vol(volume): ", volume
	else:
		volume = ""

	# Put contract into Array
	contractArr = [sGoogle, price, buy, ask, openInt, volume]
	#contractArr = [i.replace('-', 'NULL') for i in contractArr]		# replace '-' with 'NULL'
	# print contractArr
	return contractArr


def getContractString(lines):
	# input = lines of html in:  lines = ['Line1', 'Line2', 'Line3'] in a doc
	# GET PUTS & CALL contracts in a single line
	# Returns [puts, calls]
	for i in range(len(lines)):
		line = lines[i]
		# print "-----line" + str(i) + "-----:     ", line

		# find a line that contains PUTS
		if "puts:[" in line:
			# print "-----line" + str(i) + "-----:     ", line

			# GET PUTS & CALLS
			temp, puts = line.split('puts:[', 1);			#print puts 	# all puts in a single line
	 		puts, calls = puts.split('calls:[', 1);			#print calls 	# all calls in a single line
	 		calls, temp = calls.split('],', 1);				#print calls 	# chop off trailing text


	putContracts = puts.split('{cid:');			# create array of lines of contract
	putContracts = putContracts[1:];			# index=0 is null, snip it off
	for i in range(len(putContracts)):			# add "puts, " to the front of each contract"
		putContracts[i] = '"put", ' + putContracts[i]
		#print putContracts[i] , i

	callContracts = calls.split('{cid:');		# create array of lines of contract
	callContracts = callContracts[1:];			# index=0 is null, snip it off
	for i in range(len(callContracts)):			# add "puts, " to the front of each contract"
		callContracts[i] = '"call", ' + callContracts[i]
		#print callContracts[i] , i

	contracts = callContracts + putContracts

	# for i in range(len(contracts)):
	# 	print contracts[i]
	return contracts

def getExpDateStrURL(eDate):
	year = eDate[0:4]; 							#print "year: " + year
	eDate = eDate[5:];	
	month = eDate[:2];	
	month = string.replace(month, ".", "");		#print "month: " + month
	day = eDate[-2:];
	day = string.replace(day, ".", "");			#print "day: " + day
	eDate = string.replace(eDate, "{", "");					
	eDate = "&expd=" + day + "&expm=" + month + "&expy=" + year
	# print eDate
	return eDate


def main():
	# Start Parsing html file.
	tickerURL = str(sys.argv[1]);
	expDateURL = getExpDateStrURL(str(sys.argv[2]));	print expDateURL 		
	CMD = 'wget -qO- "https://www.google.com/finance/option_chain?q=' + tickerURL + expDateURL + '"'; 		print CMD
	# CMD = 'wget -qO- "https://www.google.com/finance/option_chain?q=NASDAQ:NFLX&expd=20&expm=1&expy=2017"'
	p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	# print output

	# get lines of html.  lines = ['Line1', 'Line2', 'Line3'] 
	html_lines = output.strip().split('\n')  			

	# Take lines of html and get strings of contracts
	contractsStr = getContractString(html_lines)			

	# Create a Array with basic info that will be prepended to all contract
	preArr = [getTime()] + getTickerTimestampStockPriceArr(html_lines)

	print "------------------------------------------------------------"
	# Add contract into SQLite db
	connection = sqlite3.connect(PATH + '/database/options.db')		# create connection to db
	curser = connection.cursor()							# create curser
	
	# Initializing Database
	# Uncomment the following code to DROP & CREATE a new TABLE
	# curser.executescript("""DROP TABLE IF EXISTS Options;
	# 					CREATE TABLE Options(time_sys TEXT, 
	# 										time_web TEXT, 
	# 										curr_stock_price REAL, 
	# 										contract TEXT, 
	# 										price REAL, 
	# 										buy REAL, 
	# 										ask REAL, 
	# 										open_interest INTEGER, 
	# 										volume INTEGER);
	# 					""")

	# Create a final contract Array & Put into SQLite database
	for i in range(len(contractsStr)-1):
		# execute SQLite query
		curser.execute("INSERT INTO Options VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", preArr + contractStrToArr(contractsStr[i]))	

	connection.commit()

	# Print SQLite Query
	# curser.execute("SELECT * FROM Options")
	# data = curser.fetchall()
	# for row in data:
	# 	print row


if __name__ == '__main__':
	main()
